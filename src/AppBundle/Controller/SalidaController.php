<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Salida;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Serializer\Normalizer\JsonSerializableNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
/**
 * Salida controller.
 *
 * @Route("salida")
 */
class SalidaController extends FOSRestController
{
    /**
     * Lists all salida entities.
     *
     * @Route("/", name="salida_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine();

        $salidas = $em->getRepository('AppBundle:Salida')->createQueryBuilder('q')->getQuery()->getArrayResult();

         return new JsonResponse($salidas);
    }

    /**
     * Finds and displays a salida entity.
     *
     * @Route("/{idSalida}", name="salida_show")
     * @Method("GET")
     */
    public function showAction($idSalida)
    {

        
    $salida=$this->getDoctrine()->getRepository(Salida::class)->find($idSalida);
    $salida=$this->removeCircularReference($salida);
      if($salida != null){
      $statusCode=200;
    
     $view=$this->view($salida,$statusCode);
    return  $this->handleView($view);
  }else{
      throw new HttpException(400, "salida no encontrada.");
  }
    }

    /**
     * Creates a new salida entity.
     *
     * @Route("/new", name="salida_new")
     * @ParamConverter("salida", converter="fos_rest.request_body")
     * @Method("POST")
     */
     public function newAction(Salida $salida)
    {
      $em = $this->getDoctrine()->getManager();
      $salida=$em->merge($salida);
      $validator=$this->get('validator');
      $errors=$validator->validate($salida);
      
      if(count($errors) > 0){
        $errorString=(string) $errors;
        return $errorString;
      }

      $em->persist($salida);
      $em->flush();
      $statusCode=200;
      $view=$this->view($salida,$statusCode);
              return  $this->handleView($view);
    }

     /**
     * Displays a form to edit an existing salida entity.
     *
     * @Route("/{idSalida}/edit", name="salida_edit")
     * @ParamConverter("salida", converter="fos_rest.request_body")
     * @Method("PUT")
     */
     public function editAction($idSalida,Salida $salida){
    $em = $this->getDoctrine()->getManager();
    $salida=$em->merge($salida);
    $salidaR=$this->getDoctrine()->getRepository(Salida::class)->find($idSalida);

     if($salidaR != null){
        $salidaR->setIdProgramas($salida->getIdProgramas());

         $em=$this->getDoctrine()->getManager();
        $em->persist($salidaR);
        $em->flush();

        $statusCode=200;
        $view=$this->view($salidaR,$statusCode);
        return  $this->handleView($view);
     }else{
         throw new HttpException(400, "salida no encontrada.");
     }
    }

     public function removeCircularReference($entidad){
      $encoder = new JsonEncoder();
      $normalizer = new ObjectNormalizer();
      $normalizer->setCircularReferenceLimit(1);
      $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getIdSalida();
        });
     
      $serializer = new Serializer(array($normalizer), array($encoder));
      $entidad=$serializer->serialize($entidad, 'json');
      $entidad=json_decode($entidad,true);
      return $entidad;
    }

}
