<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Institucion;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Version;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Institucion controller.
 *
 * @Route("institucion")
 */
class InstitucionController extends FOSRestController
{
    /**
     * Lists all institucion entities.
     *
     * @Route("/", name="institucion_index")
     * @Method("GET")
     */
     public function indexAction()
     {
         $em = $this->getDoctrine();

         $instituciones = $em->getRepository('AppBundle:Institucion')->createQueryBuilder('q')->getQuery()->getArrayResult();

         return new JsonResponse($instituciones);
     }

     /**
      * Creates a new donante entity.
      *
      * @Route("/new", name="institucion_new")
      * @ParamConverter("institucion", converter="fos_rest.request_body")
      * @Method("POST")
      */
      public function newAction(Institucion $institucion)
      {
        $em = $this->getDoctrine()->getManager();
        $institucion=$em->merge($institucion);
        $em->persist($institucion);
        $em->flush();

        $statusCode=200;
        $view=$this->view($institucion,$statusCode);

                return  $this->handleView($view);

      }


      /**
       * Finds and displays a institucion entity.
       *
       * @Route("/{idInstitucion}", name="institucion_show")
       * @Method("GET")
       */
      public function showAction($idInstitucion){

      $institucion=$this->getDoctrine()->getRepository(Institucion::class)->find($idInstitucion);
        if($institucion != null){
        $statusCode=200;


        $view=$this->view($institucion,$statusCode);
      return  $this->handleView($view);
    }else{
        throw new HttpException(400, "Institucion no encontrada.");
    }
    }

    /**
     * Displays a form to edit an existing institucion entity.
     *
     * @Route("/{idInstitucion}/edit", name="institucion_edit")
     * @ParamConverter("institucion", converter="fos_rest.request_body")
     * @Method("PUT")
     */
    public function editAction($idInstitucion, Institucion $institucion)
    {
      $institucionR=$this->getDoctrine()->getRepository(Institucion::class)->find($idInstitucion);

      if($institucionR != null){
        $institucionR->setNombreInstitucion($institucion->getNombreInstitucion());
        $institucionR->setDireccion($institucion->getDireccion());
        $institucionR->setCorreo($institucion->getCorreo());
        $institucionR->setTipoInstitucion($institucion->getTipoInstitucion());
        $institucionR->setIdPersona($institucion->getIdPersona());

        $em=$this->getDoctrine()->getManager();
          $em->persist($institucionR);
          $em->flush();

          $statusCode=200;
          $view=$this->view($institucionR,$statusCode);
          return  $this->handleView($view);
        }else{
            throw new HttpException(400, "Institucion no encontrada.");
        }

    }

    /**
     * Deletes a institucion entity.
     *
     * @Route("/{idInstitucion}", name="institucion_delete")
     * @Method("DELETE")
     */
    public function deleteAction($idInstitucion)
    {
      $institucion=$this->getDoctrine()->getRepository(Institucion::class)->find($idInstitucion);
        if($institucion != null){

            $em = $this->getDoctrine()->getManager();
            $em->remove($institucion);
            $em->flush();
        return new JsonResponse("Borrado con exito");
      }else{
      throw new HttpException(400, "institucion no encontrada.");
      }
    }

  
}
