<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Donacion;
use AppBundle\Entity\Salida;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Donacion controller.
 *
 * @Route("donacion")
 */
class DonacionController extends FOSRestController
{
    /**
     * Lists all donacion entities.
     *
     * @Route("/", name="donacion_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine();

        $donaciones = $em->getRepository('AppBundle:Donacion')->createQueryBuilder('q')->getQuery()->getArrayResult();

       return new JsonResponse($donaciones);
        
    }

    /**
     * Finds and displays a donacion entity.
     *
     * @Route("/{idDonacion}", name="donacion_show")
     * @Method("GET")
     */
    public function showAction($idDonacion)
    {

      $donacion=$this->getDoctrine()->getRepository(Donacion::class)->find($idDonacion);
      $donacion=$this->removeCircularReference($donacion);


      if($donacion != null){
      $statusCode=200;
    
     $view=$this->view($donacion,$statusCode);
        return  $this->handleView($view);
  }else{
      throw new HttpException(400, "Donacion no encontrada.");
  }
    }



    /**
     * Creates a new donacion entity.
     *
     * @Route("/new", name="donacion_new")
     * @ParamConverter("donacion", converter="fos_rest.request_body")
     * @Method("POST")
     */
    public function newAction(Donacion $donacion)
    {
      $em = $this->getDoctrine()->getManager();
      
      $donacion=$em->merge($donacion);
      $validator=$this->get('validator');
      $errors=$validator->validate($donacion);

      
      if(count($errors) > 0){
        $errorString=(string) $errors;
        return $errorString;
      }

      $em->persist($donacion);
      $em->flush();

     $donacion=$this->removeCircularReference($donacion);

      $statusCode=200;
      $view=$this->view($donacion,$statusCode);

              return  $this->handleView($view);
    }

     /**
     * Displays a form to edit an existing donacion entity.
     *
     * @Route("/{idDonacion}/edit", name="donacion_edit")
     * @ParamConverter("donacion", converter="fos_rest.request_body")
     * @Method("PUT")
     */
     public function editAction($idDonacion,Donacion $donacion){
    $em = $this->getDoctrine()->getManager();
    $donacion=$em->merge($donacion);
    $donacionR=$this->getDoctrine()->getRepository(Donacion::class)->find($idDonacion);

     if($donacionR != null){
        $donacionR->setMonto($donacion->getMonto());
        $donacionR->setIdPersona($donacion->getIdPersona());

        $em=$this->getDoctrine()->getManager();
        $em->persist($donacionR);
        $em->flush();
        $donacionR=$this->removeCircularReference($donacionR);


        $statusCode=200;
        $view=$this->view($donacionR,$statusCode);
        return  $this->handleView($view);
     }else{
         throw new HttpException(400, "Donacion no encontrada.");
     }
    }

    /**
    *
    *@Route("/{idDonacion}/addSalida", name="donacion_addSalida")
    *@ParamConverter("salida", converter="fos_rest.request_body")
    *@Method("PATCH")
    */
    public function addSalida($idDonacion,Salida $salida){
      $em = $this->getDoctrine()->getManager();
    $salida=$em->merge($salida);
    $donacion=$this->getDoctrine()->getRepository(Donacion::class)->find($idDonacion);
     if($donacion != null){
      $donacion->addIdSalida($salida);
      $salida->addIdDonacion($donacion);
      $em=$this->getDoctrine()->getManager();
        $em->persist($donacion);
        $em->flush();
        $em=$this->getDoctrine()->getManager();
        $em->persist($salida);
        $em->flush();

        $donacion=$this->removeCircularReference($donacion);
       $statusCode=200;
        $view=$this->view($donacion,$statusCode);
         return  $this->handleView($view);
   }else{
         throw new HttpException(400, "Donacion no encontrada.");
     }
    }


public function removeCircularReference($entidad){
      $encoder = new JsonEncoder();
      $normalizer = new ObjectNormalizer();

      $normalizer->setCircularReferenceHandler(function ($object) {
            return $object -> getIdPersona();
             
        });

      $serializer = new Serializer(array($normalizer), array($encoder));
      $entidad=$serializer->serialize($entidad, 'json');
      $entidad=json_decode($entidad,true);
      return $entidad;
}
}


