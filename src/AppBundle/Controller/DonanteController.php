<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Donante;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Version;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Serializer\Normalizer\JsonSerializableNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
/**
 * Donante controller.
 *
 * @Route("donante")
 */
class DonanteController extends FOSRestController
{
  /**
   * Lists all Donante entities.
   *
   * @Route("/", name="donante_index")
   * @Method("GET")
   */
  public function indexAction()
  {
      $em = $this->getDoctrine();
      $donantes = $em->getRepository('AppBundle:Donante')->findAll();
      $donantes=$this->removeCircularReference($donantes);
      $statusCode=200;
      $view=$this->view($donantes,$statusCode);

      return $view;
  }

    /**
     * Creates a new donante entity.
     *
     * @Route("/new", name="donante_new")
     * @ParamConverter("donante", converter="fos_rest.request_body")
     * @Method("POST")
     */
    public function newAction(Donante $donante)
    {
      $em = $this->getDoctrine()->getManager();
      $donante=$em->merge($donante);
      $validator=$this->get('validator');
      $errors=$validator->validate($donante);
      
      if(count($errors) > 0){
        $errorString=(string) $errors;
        return $errorString;
      }

      $em->persist($donante);
      $em->flush();
      $donante=$this->removeCircularReference($donante);
      $statusCode=200;
      $view=$this->view($donante,$statusCode);

      return  $this->handleView($view);
}


    /**
     * Finds and displays a donante entity.
     *
     * @Route("/{idPersona}", name="donante_show")
     * @Method("GET")
     */
  public function showAction($idPersona){

      $donante=$this->getDoctrine()->getRepository(Donante::class)->find($idPersona);
      $donante=$this->removeCircularReference($donante);
    
      if($donante != null){
      $statusCode=200;
      $view=$this->view($donante,$statusCode);
            return  $this->handleView($view);

      }else{
          throw new HttpException(400, "Donante no encontrado.");
      }
    }


    /**
     * Displays a form to edit an existing donante entity.
     *
     * @Route("/{idPersona}/edit", name="donante_edit")
     * @ParamConverter("donante", converter="fos_rest.request_body")
     * @Method("PUT")
     */
    public function editAction($idPersona,Donante $donante)
    {
       $em = $this->getDoctrine()->getManager();
       $donante=$em->merge($donante);
      $donanteR=$this->getDoctrine()->getRepository(Donante::class)->find($idPersona);

      if($donanteR != null){
      $donanteR->setNombre($donante->getNombre());
      $donanteR->setIdInstitucion($donante->getIdInstitucion());
      $donanteR->setApellido($donante->getApellido());
      $donanteR->setDui($donante->getDui());
      $donanteR->setNit($donante->getTelefono());
      $donanteR->setCargo($donante->getCargo());
      $donanteR->setCorreoElectronico($donante->getCorreoElectronico());
      $donanteR->setEstado($donante->getEstado());
      $donanteR->setIdInstitucion($donante->getIdInstitucion());
      $em=$this->getDoctrine()->getManager();
        $em->persist($donanteR);
        $em->flush();
        $donanteR=$this->removeCircularReference($donanteR);
        $statusCode=200;
        $view=$this->view($donanteR,$statusCode);
        return  $this->handleView($view);
      }else{
          throw new HttpException(400, "Donante no encontrado.");
      }

    }

    /**
     * Deletes a donante entity.
     *
     * @Route("/{idPersona}", name="donante_delete")
     * @Method("DELETE")
     */
    public function deleteAction($idPersona){
      $donante=$this->getDoctrine()->getRepository(Donante::class)->find($idPersona);
        if($donante != null){
            $em = $this->getDoctrine()->getManager();
            $em->remove($donante);
            $em->flush();
        return new JsonResponse("Borrado con exito");
      }else{
      throw new HttpException(400, "Donante no encontrado.");
      }
    }

    public function removeCircularReference($entidad){
      $encoder = new JsonEncoder();
      $normalizer = new ObjectNormalizer();
      $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getIdPersona();
        });
      $serializer = new Serializer(array($normalizer), array($encoder));
      $entidad=$serializer->serialize($entidad, 'json');
      $entidad=json_decode($entidad,true);
      return $entidad;
    }


}
