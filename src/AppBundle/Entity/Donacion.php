<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Donacion
 *
 * @ORM\Table(name="donacion", uniqueConstraints={@ORM\UniqueConstraint(name="donacion_pk", columns={"id_donacion"})}, indexes={@ORM\Index(name="relationship_19_fk", columns={"id_persona"})})
 * @ORM\Entity
 */
class Donacion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_donacion", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="donacion_id_donacion_seq", allocationSize=1, initialValue=1)
     */
    private $idDonacion;

    /**
     * @var string
     *
     * @ORM\Column(name="monto", type="decimal", precision=7, scale=2, nullable=false)
     */
    private $monto;

   
   
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="string", length=50, nullable=false)
     */
    private $fecha;

    /**
     * @var \Donante
     *
     * @ORM\ManyToOne(targetEntity="Donante", inversedBy="donaciones")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_persona", referencedColumnName="id_persona")
     * })
     */
    private $idPersona;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Salida", inversedBy="idDonacion")
     * @ORM\JoinTable(name="relationship_15",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_donacion", referencedColumnName="id_donacion")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_salida", referencedColumnName="id_salida")
     *   }
     * )
     */
    private $idSalida;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Actividad", inversedBy="idDonacion")
     * @ORM\JoinTable(name="relationship_16",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_donacion", referencedColumnName="id_donacion")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_actividad", referencedColumnName="id_actividad")
     *   }
     * )
     */
    private $idActividad;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idSalida = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idActividad = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set idDonacion
     *
     * @param integer $idDonacion
     *
     * @return Donacion
     */
    public function setIdDonacion($idDonacion)
    {
        $this->idDonacion = $idDonacion;

        return $this;
    }

    /**
     * Get idDonacion
     *
     * @return integer
     */
    public function getIdDonacion()
    {
        return $this->idDonacion;
    }

    /**
     * Set monto
     *
     * @param string $monto
     *
     * @return Donacion
     */
    public function setMonto($monto)
    {
        $this->monto = $monto;
    
        return $this;
    }

    /**
     * Get monto
     *
     * @return string
     */
    public function getMonto()
    {
        return $this->monto;
    }

    /**
     * Set idPersona
     *
     * @param \AppBundle\Entity\Donante $idPersona
     *
     * @return Donacion
     */
    public function setIdPersona(\AppBundle\Entity\Donante $idPersona = null)
    {
        $this->idPersona = $idPersona;
    
        return $this;
    }


    /**
     * Get monto
     *
     * @return \AppBundle\Entity\Donante
     */
    public function getIdPersona(){
        return $this->idPersona;
    }

     /**
     *
     * @param \DateTime $fecha
     *
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }
   

    /**
     * Add idSalida
     *
     * @param \AppBundle\Entity\Salida $idSalida
     *
     * @return Donacion
     */
    public function addIdSalida(\AppBundle\Entity\Salida $idSalida)
    {
        $idSalida->addIdDonacion($this);
        $this->idSalida[] = $idSalida;
    
        return $this;
    }

    /**
     * Remove idSalida
     *
     * @param \AppBundle\Entity\Salida $idSalida
     */
    public function removeIdSalida(\AppBundle\Entity\Salida $idSalida)
    {
        $this->idSalida->removeElement($idSalida);
    }

    /**
     * Get idSalida
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdSalida()
    {
        return $this->idSalida;
    }

    /**
     * Set idSalida
     *
     * @param \Doctrine\Common\Collections\Collection
     *
     * @return Donacion
     */
    public function setIdSalida( $idSalida ){
        $this->idSalida=$idSalida;
        return $this;
    }

    /**
     * Add idActividad
     *
     * @param \AppBundle\Entity\Actividad $idActividad
     *
     * @return Donacion
     */
    public function addIdActividad(\AppBundle\Entity\Actividad $idActividad)
    {
        $this->idActividad[] = $idActividad;
    
        return $this;
    }

    /**
     * Remove idActividad
     *
     * @param \AppBundle\Entity\Actividad $idActividad
     */
    public function removeIdActividad(\AppBundle\Entity\Actividad $idActividad)
    {
        $this->idActividad->removeElement($idActividad);
    }

    /**
     * Get idActividad
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdActividad()
    {
        return $this->idActividad;
    }


}
