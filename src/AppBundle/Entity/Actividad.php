<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Actividad
 *
 * @ORM\Table(name="actividad", uniqueConstraints={@ORM\UniqueConstraint(name="actividad_pk", columns={"id_actividad"})})
 * @ORM\Entity
 */
class Actividad
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_actividad", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="actividad_id_actividad_seq", allocationSize=1, initialValue=1)
     */
    private $idActividad;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_actividad", type="string", length=30, nullable=false)
     */
    private $nombreActividad;

    /**
     * @var string
     *
     * @ORM\Column(name="costo_actividad", type="decimal", precision=7, scale=2, nullable=true)
     */
    private $costoActividad;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Donacion", mappedBy="idActividad")
     */
    private $idDonacion;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idDonacion = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get idActividad
     *
     * @return integer
     */
    public function getIdActividad()
    {
        return $this->idActividad;
    }

    /**
     * Set nombreActividad
     *
     * @param string $nombreActividad
     *
     * @return Actividad
     */
    public function setNombreActividad($nombreActividad)
    {
        $this->nombreActividad = $nombreActividad;
    
        return $this;
    }

    /**
     * Get nombreActividad
     *
     * @return string
     */
    public function getNombreActividad()
    {
        return $this->nombreActividad;
    }

    /**
     * Set costoActividad
     *
     * @param string $costoActividad
     *
     * @return Actividad
     */
    public function setCostoActividad($costoActividad)
    {
        $this->costoActividad = $costoActividad;
    
        return $this;
    }

    /**
     * Get costoActividad
     *
     * @return string
     */
    public function getCostoActividad()
    {
        return $this->costoActividad;
    }

    /**
     * Add idDonacion
     *
     * @param \AppBundle\Entity\Donacion $idDonacion
     *
     * @return Actividad
     */
    public function addIdDonacion(\AppBundle\Entity\Donacion $idDonacion)
    {
        $this->idDonacion[] = $idDonacion;
    
        return $this;
    }

    /**
     * Remove idDonacion
     *
     * @param \AppBundle\Entity\Donacion $idDonacion
     */
    public function removeIdDonacion(\AppBundle\Entity\Donacion $idDonacion)
    {
        $this->idDonacion->removeElement($idDonacion);
    }

    /**
     * Get idDonacion
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdDonacion()
    {
        return $this->idDonacion;
    }
}
