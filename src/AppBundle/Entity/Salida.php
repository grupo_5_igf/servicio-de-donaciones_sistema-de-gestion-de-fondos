<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Salida
 *
 * @ORM\Table(name="salida", uniqueConstraints={@ORM\UniqueConstraint(name="salida_pk", columns={"id_salida"})})
 * @ORM\Entity
 */
class Salida
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_salida", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="salida_id_salida_seq", allocationSize=1, initialValue=1)
     */
    private $idSalida;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_programas", type="integer", nullable=true)
     */
    private $idProgramas;

   /**
     * @var string
     *
     * @ORM\Column(name="monto", type="decimal", precision=7, scale=2, nullable=false)
     */
    private $montoSalida;


    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Donacion", mappedBy="idSalida")
     */
    private $idDonacion;



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idDonacion = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set idSalida
     *
     * @param integer $idSalida
     *
     * @return Salida
     */
    public function setIdSalida($idSalida)
    {
        $this->idSalida = $idSalida;

        return $this;
    }

    /**
     * Get idSalida
     *
     * @return integer
     */
    public function getIdSalida()
    {
        return $this->idSalida;
    }

    /**
     * Set idProgramas
     *
     * @param integer $idProgramas
     *
     * @return Salida
     */
    public function setIdProgramas($idProgramas)
    {
        $this->idProgramas = $idProgramas;
    
        return $this;
    }

    /**
     * Get idProgramas
     *
     * @return integer
     */
    public function getIdProgramas()
    {
        return $this->idProgramas;
    }


/**
     * Set monto
     *
     * @param decimal $montoSalida
     *
     * @return Salida
     */
    public function setMontoSalida($montoSalida)
    {
        $this->montoSalida = $montoSalida;
    
        return $this;
    }


    /**
     * Get monto
     *
     * @return decimal
     */
    public function getMontoSalida()
    {
        return $this->montoSalida;
    }


    /**
     * Add idDonacion
     *
     * @param \AppBundle\Entity\Donacion $idDonacion
     *
     * @return Salida
     */
    public function addIdDonacion(\AppBundle\Entity\Donacion $idDonacion)
    {
        $this->idDonacion[] = $idDonacion;
    
        return $this;
    }

    /**
     * Remove idDonacion
     *
     * @param \AppBundle\Entity\Donacion $idDonacion
     */
    public function removeIdDonacion(\AppBundle\Entity\Donacion $idDonacion)
    {
        $this->idDonacion->removeElement($idDonacion);
    }

   
}
