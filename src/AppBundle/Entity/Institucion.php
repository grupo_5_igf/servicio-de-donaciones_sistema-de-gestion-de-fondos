<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Institucion
 *
 * @ORM\Table(name="institucion", uniqueConstraints={@ORM\UniqueConstraint(name="institucion_pk", columns={"id_institucion"})}, indexes={@ORM\Index(name="relationship_11_fk", columns={"id_persona"})})
 * @ORM\Entity
 */
class Institucion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_institucion", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="institucion_id_institucion_seq", allocationSize=1, initialValue=1)
     */
    private $idInstitucion;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_institucion", type="string", length=50, nullable=true)
     */
    private $nombreInstitucion;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=50, nullable=true)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="correo", type="string", length=30, nullable=true)
     */
    private $correo;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo_institucion", type="string", length=10, nullable=true)
     */
    private $tipoInstitucion;

    /**
     * @var integer
     *
      * @ORM\Column(name="id_persona", type="integer", nullable=true)
     * })
     */
    private $idPersona;


    /**
     * Set idInstitucion
     *
     * @param integer $idInstitucion
     *
     * @return Institucion
     */
    public function setIdInstitucion($idInstitucion)
    {
        $this->idInstitucion = $idInstitucion;

        return $this;
    }


    /**
     * Get idInstitucion
     *
     * @return integer
     */
    public function getIdInstitucion()
    {
        return $this->idInstitucion;
    }

    /**
     * Set nombreInstitucion
     *
     * @param string $nombreInstitucion
     *
     * @return Institucion
     */
    public function setNombreInstitucion($nombreInstitucion)
    {
        $this->nombreInstitucion = $nombreInstitucion;

        return $this;
    }

    /**
     * Get nombreInstitucion
     *
     * @return string
     */
    public function getNombreInstitucion()
    {
        return $this->nombreInstitucion;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     *
     * @return Institucion
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set correo
     *
     * @param string $correo
     *
     * @return Institucion
     */
    public function setCorreo($correo)
    {
        $this->correo = $correo;

        return $this;
    }

    /**
     * Get correo
     *
     * @return string
     */
    public function getCorreo()
    {
        return $this->correo;
    }

    /**
     * Set tipoInstitucion
     *
     * @param string $tipoInstitucion
     *
     * @return Institucion
     */
    public function setTipoInstitucion($tipoInstitucion)
    {
        $this->tipoInstitucion = $tipoInstitucion;

        return $this;
    }

    /**
     * Get tipoInstitucion
     *
     * @return string
     */
    public function getTipoInstitucion()
    {
        return $this->tipoInstitucion;
    }

    /**
     * Set idPersona
     *
     * @param integer
     *
     * @return Institucion
     */
    public function setIdPersona($idPersona)
    {
        $this->idPersona = $idPersona;

        return $this;
    }

    /**
     * Get idPersona
     *
     * @return integer
     */
    public function getIdPersona()
    {
        return $this->idPersona;
    }
}